// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class SimpleMMORPG_ClientTarget : TargetRules
{
	public SimpleMMORPG_ClientTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "SimpleMMORPG_Client" } );
	}
}
