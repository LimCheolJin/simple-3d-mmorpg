// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SimpleMMORPG_Client.h"
#include "GameFramework/Actor.h"
#include "Protocol.h"
#include "NetworkActor.generated.h"

UCLASS()
class SIMPLEMMORPG_CLIENT_API ANetworkActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ANetworkActor();
	//ANetworkActor(const ANetworkActor& networkActor) = delete;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	bool InitializeNetwork();

	void SendPacket(void* packet);
	void SendLoginPacket(FString id);
	void SendLogoutPacket();
	

	void RecvPacketConstruct(int32 recvbytes);
	void PacketProcess(uint8* buf);
private:
	class FSocket* m_ClientSocket; //클라이언트 소켓

	uint8 m_RecvBuffer[MAXBUFFERSIZE];
	uint8 m_SendBuffer[MAXBUFFERSIZE];

	uint8 m_PacketBuffer[MAXBUFFERSIZE];
	int m_PrevSize = 0;

	FString m_MyID;
	int m_MyIndex;
};
