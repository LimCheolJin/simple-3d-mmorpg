// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EngineMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(SimpleMMORPG_Client, Log, All);

#define MMLOG_CALLINFO (FString(__FUNCTION__) + TEXT("(") + FString::FromInt(__LINE__) + TEXT(")") )
#define MMLOG_S(Verbosity) UE_LOG(SimpleMMORPG_Client, Verbosity, TEXT("%s"), *MMLOG_CALLINFO)
#define MMLOG(Verbosity, Format, ...) UE_LOG(SimpleMMORPG_Client, Verbosity, TEXT("%s %s"), *MMLOG_CALLINFO, *FString::Printf(Format, ##__VA_ARGS__))
#define MMCHECK(Expr, ...) {if(!(Expr)){MMLOG(Error, TEXT("ASSERTION : %s"), TEXT("'"#Expr"'"));return __VA_ARGS__;}}