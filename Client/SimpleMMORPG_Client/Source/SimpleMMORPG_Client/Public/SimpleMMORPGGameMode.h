// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SimpleMMORPG_Client.h"
#include "GameFramework/GameModeBase.h"
#include "SimpleMMORPGGameMode.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLEMMORPG_CLIENT_API ASimpleMMORPGGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;

public:
	ASimpleMMORPGGameMode();
	~ASimpleMMORPGGameMode();

	UFUNCTION(BlueprintCallable)
	void SetMyIDText(FString id);

private:
	class ANetworkActor* m_NetworkActor;
	class FString m_ID;
};
