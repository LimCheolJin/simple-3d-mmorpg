#pragma once
constexpr int MAXBUFFERSIZE = 4096;
constexpr int MAXUSER = 10000;
constexpr int MAXNAMESIZE = 50;
constexpr int MAXCHATSIZE = 128;

enum class e_packet_type : unsigned char
{
	LOGIN_PACKET,
	LOGIN_FAIL,
	LOGOUT_PACKET,
	LOGIN_SUCCEED
};

#pragma pack(push, 1) //1바이트 단위로 패딩
struct CS_LOGIN_PACKET
{
	char size;
	e_packet_type type;
	wchar_t id[MAXNAMESIZE];
};
struct CS_LOGOUT_PACKET
{
	char size;
	e_packet_type type;
};


struct SC_LOGIN_FAIL_PACKET
{
	char size;
	e_packet_type type;
};

struct SC_LOGIN_SUCCEED_PACKET
{
	char size;
	e_packet_type type;
	int user_id;
};
#pragma pack(pop)