// Fill out your copyright notice in the Description page of Project Settings.


#include "NetworkActor.h"
#include "Networking.h"
#include "Sockets.h"
#include "IPAddress.h"
#include "Runtime/Networking/Public/Interfaces/IPv4/IPv4Address.h"
#include "SocketSubsystem.h"
#include "Protocol.h"
#include "HAL/UnrealMemory.h"
#include "Blueprint/WidgetLayoutLibrary.h"

// Sets default values
ANetworkActor::ANetworkActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ANetworkActor::BeginPlay()
{
	Super::BeginPlay();
	

	if (false == InitializeNetwork())
	{
		UE_LOG(LogTemp, Error, TEXT("ANetworkActor's InitializeNetWork() Error"));
		return;
	}


	//m_ClientSocket->Connect(FInternetAddr("127.0.0.1"));
}

bool ANetworkActor::InitializeNetwork()
{
	/*FSocket* Socket = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateSocket(NAME_Stream, TEXT("default"), false);

	FString address = TEXT("127.0.0.1");
	int32 port = 8989;
	FIPv4Address ip;
	FIPv4Address::Parse(address, ip);

	TSharedRef<FInternetAddr> addr = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	addr->SetIp(ip.Value);
	addr->SetPort(port);

	

	bool connected = Socket->Connect(*addr);*/


	//루프백주소
	FString myAddr  = TEXT( "127.0.0.1" );
	
	//포트 9000
	int32 myPort{ 9000 };

	m_ClientSocket = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateSocket(NAME_Stream, myAddr, false);
	if (m_ClientSocket == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("ClientSocket is nullptr"));
		return false;
	}
	//set
	FIPv4Address myIPV4{};
	FIPv4Address::Parse(myAddr, myIPV4);

	TSharedRef<FInternetAddr> myNetworkInfo = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	myNetworkInfo->SetIp(myIPV4.Value);
	myNetworkInfo->SetPort(myPort);
		
	//connect
	if(! m_ClientSocket->Connect(*myNetworkInfo) )
		return false;

	//set nonblocking
	if (!m_ClientSocket->SetNonBlocking(true))
		return false;

	return true;
}

// Called every frame
void ANetworkActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	int32 recvbytes = 0;
	m_ClientSocket->Recv(m_RecvBuffer, MAXBUFFERSIZE, recvbytes);
	if(recvbytes > 0)
		RecvPacketConstruct(recvbytes);
}

void ANetworkActor::SendLoginPacket(FString id)
{//유니코드로 인해 문제가 되는거 같다. 그래서 이부분만 sendbuffer를 사용하지 않고 보내겠다.
	auto wideID = TCHAR_TO_WCHAR(*id);

	//TCHAR_TO_UTF8
	CS_LOGIN_PACKET p{};
	p.size = sizeof(CS_LOGIN_PACKET);
	wcscpy(p.id, wideID);
	p.type = e_packet_type::LOGIN_PACKET;
	SendPacket(&p);

	//set my id
	m_MyID = id;
}

void ANetworkActor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	SendLogoutPacket();
	UE_LOG(LogTemp, Error, TEXT("I AM LOGOUT STATUS!!"));
}

void ANetworkActor::SendPacket(void* packet)
{
	char* p = reinterpret_cast<char*>(packet);
	memcpy(m_SendBuffer, p, p[0]);

	int sentbytes = 0;
	if (!m_ClientSocket->Send(m_SendBuffer, p[0], sentbytes))
	{
		//실패
		UE_LOG(LogTemp, Error, TEXT("SendPacket Error !!"));
	}
	else
	{
		//성공
		UE_LOG(LogTemp, Error, TEXT("Sendbytes are %d !!"), sentbytes);
	}
}

void ANetworkActor::RecvPacketConstruct(int32 recvbytes)
{
	int rest_bytes = recvbytes;
	uint8* p = m_RecvBuffer;//m_PacketBuffer;
	int packet_size = 0;
	if (p[1] <= 0) return; // size를 음수값으로 보낼경우 이상한패킷임

	if (0 != m_PrevSize) packet_size = m_PacketBuffer[0];
	while (rest_bytes > 0) // 아직처리할 데이터가 남아있으면.
	{
		if (0 == packet_size)
			packet_size = *p;
		if (packet_size <= rest_bytes + m_PrevSize) //지난번에 받은거랑 지금받은거랑 합쳣을때 패킷사이즈보다 크면 완성가능
		{
			memcpy(m_PacketBuffer + m_PrevSize, p, packet_size - m_PrevSize);
			p += packet_size - m_PrevSize;
			rest_bytes -= packet_size - m_PrevSize;
			packet_size = 0;
			PacketProcess(m_PacketBuffer);
			m_PrevSize = 0;
		}
		else //완성못하면 저장을해야함.
		{
			memcpy(m_PacketBuffer + m_PrevSize, p, rest_bytes);
			m_PrevSize += rest_bytes; // 추가해줌 이전에받았던 데이터사이즈를
			rest_bytes = 0;
			p += rest_bytes;

		}
	}
}
void ANetworkActor::PacketProcess(uint8* buf)
{
	switch ((e_packet_type)buf[1])
	{
	case e_packet_type::LOGIN_SUCCEED:
	{
		SC_LOGIN_SUCCEED_PACKET* p = reinterpret_cast<SC_LOGIN_SUCCEED_PACKET*>(buf);
		m_MyIndex = p->user_id;
		UE_LOG(LogTemp, Error, TEXT("Login Succeed"));

		UWidgetLayoutLibrary::RemoveAllWidgets(GetWorld());

		//다음 SCENE으로 진입해야한다.(character선택 및 새로운 캐릭터 생성하는 level)
		UGameplayStatics::LoadStreamLevel(GetWorld(), FName("CharacterSelectAndmakeNewCharacterLevel"), true, false, FLatentActionInfo{});
		UGameplayStatics::UnloadStreamLevel(GetWorld(), FName("LoginLevel"), FLatentActionInfo{}, true);
	}
	break;
	case e_packet_type::LOGIN_FAIL:
	{ // 재로그인하라는 UI를 출력해야한다. 

	}
	break;
	default:
		UE_LOG(LogTemp, Error, TEXT("Unknown packet type"));
		__debugbreak();
		break;
	}
}

void ANetworkActor::SendLogoutPacket()
{
	CS_LOGOUT_PACKET p{};
	p.size = sizeof(CS_LOGOUT_PACKET);
	p.type = e_packet_type::LOGOUT_PACKET;

	SendPacket(&p);
}