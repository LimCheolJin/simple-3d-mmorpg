// Fill out your copyright notice in the Description page of Project Settings.


#include "SimpleMMORPGGameMode.h"
#include "NetworkActor.h"

ASimpleMMORPGGameMode::ASimpleMMORPGGameMode()
{

}
ASimpleMMORPGGameMode::~ASimpleMMORPGGameMode()
{

}
void ASimpleMMORPGGameMode::BeginPlay()
{
	Super::BeginPlay();
	TArray<AActor*> tempactors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ANetworkActor::StaticClass(), tempactors);
	if (tempactors.IsValidIndex(0)) // 0번째 인덱스가 트루이면
	{
		m_NetworkActor = Cast<ANetworkActor>(tempactors[0]);
	}

	//UGameplayStatics::LoadStreamLevel(this, FName("LoginLevel"),true, true, FLatentActionInfo{});

	//UGameplayStatics::UnloadStreamLevel(this, FName("PersistentLevel"), FLatentActionInfo{}, true);
}


void ASimpleMMORPGGameMode::SetMyIDText(FString id)
{
	m_ID = id;
	if (!m_NetworkActor) return;

	m_NetworkActor->SendLoginPacket(m_ID);
}