// Fill out your copyright notice in the Description page of Project Settings.

#include "SimpleMMORPG_Client.h"
#include "Modules/ModuleManager.h"

DEFINE_LOG_CATEGORY(SimpleMMORPG_Client);
IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SimpleMMORPG_Client, "SimpleMMORPG_Client" );
