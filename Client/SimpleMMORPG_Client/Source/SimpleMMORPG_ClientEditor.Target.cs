// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class SimpleMMORPG_ClientEditorTarget : TargetRules
{
	public SimpleMMORPG_ClientEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "SimpleMMORPG_Client" } );
	}
}
