#include "DataBase.h"



DataBase::DataBase()
{
}


DataBase::~DataBase()
{
	SQLFreeHandle(SQL_HANDLE_DBC, m_hdbc);
	SQLFreeHandle(SQL_HANDLE_ENV, m_henv);
}

void DataBase::InsertUser(std::wstring name)
{
	m_retcode = SQLAllocHandle(SQL_HANDLE_STMT, m_hdbc, &m_hstmt);

	SQLLEN cbPosX = 0, cbPosY = 0;

	wchar_t text[300];
	wsprintf(text, L"EXEC InsertUser %s", (TCHAR*)name.c_str() );

	m_retcode = SQLExecDirect(m_hstmt, (SQLWCHAR*)text, SQL_NTS);
	if (m_retcode == SQL_SUCCESS || m_retcode == SQL_SUCCESS_WITH_INFO)
	{
		//std::cout << "삽입 완료 ";
		//wcout << m_User_Name << L" : " << x << L", " << y << L", " << hp << L", " << exp << L", " << level << L"\n";
	}
	else {
		error_print_DB(m_hstmt, SQL_HANDLE_STMT, m_retcode);
	}

	// Process data  
	if (m_retcode == SQL_SUCCESS || m_retcode == SQL_SUCCESS_WITH_INFO)
	{
		SQLCancel(m_hstmt);
		SQLFreeHandle(SQL_HANDLE_STMT, m_hstmt);
	}
}
void DataBase::GetUserInfo(std::wstring name, int& worldinfo)
{
	m_retcode = SQLAllocHandle(SQL_HANDLE_STMT, m_hdbc, &m_hstmt);
	wchar_t text[300];


	wsprintf(text, L"EXEC GetUserInfo %s", (TCHAR*)name.c_str());
	m_retcode = SQLExecDirect(m_hstmt, (SQLWCHAR*)text, SQL_NTS);
	if (m_retcode == SQL_SUCCESS || m_retcode == SQL_SUCCESS_WITH_INFO)
	{
		m_retcode = SQLBindCol(m_hstmt, 1, SQL_C_WCHAR, m_User_Name, MAXNAMESIZE, &cbName);
		m_retcode = SQLBindCol(m_hstmt, 2, SQL_C_LONG, &worldinfo, 4, &cbWorldMap);

		// Fetch and print each row of data. On an error, display a message and exit.  
		for (int i = 0; ; i++) 
		{
			m_retcode = SQLFetch(m_hstmt);
			if (m_retcode == SQL_ERROR)
				std::cout << "error\n";
			if (m_retcode == SQL_SUCCESS || m_retcode == SQL_SUCCESS_WITH_INFO)
			{
				//std::wcout << L"DB GET USER SUCCEED \n";
				//wcout << L"DB Get " << m_User_Name << L" : " << x << L", " << y << L", " << hp << L", " << exp << L", " << level << L"\n";
				//wprintf(L"Coord: %d %d\n", x, y);
			}
			else
				break;
		}
	}
	else
	{
		error_print_DB(m_hstmt, SQL_HANDLE_STMT, m_retcode);
	}
	// Process data  
	if (m_retcode == SQL_SUCCESS || m_retcode == SQL_SUCCESS_WITH_INFO)
	{
		SQLCancel(m_hstmt);
		SQLFreeHandle(SQL_HANDLE_STMT, m_hstmt);
	}
}
void DataBase::UpdateUserInfo(std::wstring name, int worldinfo)
{
	//std::cout << "update db \n";
	m_retcode = SQLAllocHandle(SQL_HANDLE_STMT, m_hdbc, &m_hstmt);
	wchar_t text[300];
	//TCHAR* wchar_id = (TCHAR*)name;
	wsprintf(text, L"EXEC UpdateUserInfo %s, %d", (TCHAR*)name.c_str(), worldinfo);

	m_retcode = SQLExecDirect(m_hstmt, (SQLWCHAR*)text, SQL_NTS);
	if (m_retcode == SQL_SUCCESS || m_retcode == SQL_SUCCESS_WITH_INFO)
	{
		//std::cout << "업데이트 완료 DB Set ";
		//wcout << m_User_Name << L" : " << x << L", " << y << L", " << hp << L", " << exp << L", " << level << L"\n";
	}
	else {
		error_print_DB(m_hstmt, SQL_HANDLE_STMT, m_retcode);
	}

	// Process data  
	if (m_retcode == SQL_SUCCESS || m_retcode == SQL_SUCCESS_WITH_INFO)
	{
		SQLCancel(m_hstmt);
		SQLFreeHandle(SQL_HANDLE_STMT, m_hstmt);
	}
}

void DataBase::error_print_DB(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode)
{
	SQLSMALLINT iRec = 0;
	SQLINTEGER iError;
	WCHAR wszMessage[1000];
	WCHAR wszState[SQL_SQLSTATE_SIZE + 1];
	if (RetCode == SQL_INVALID_HANDLE) 
	{
		fwprintf(stderr, L"Invalid handle!\n");
		return;
	}
	while (SQLGetDiagRec(hType, hHandle, ++iRec, wszState, &iError, wszMessage,
		(SQLSMALLINT)(sizeof(wszMessage) / sizeof(WCHAR)), (SQLSMALLINT*)NULL) == SQL_SUCCESS)
	{
		// Hide data truncated..
		if (wcsncmp(wszState, L"01004", 5)) 
		{
			fwprintf(stderr, L"[%5.5s] %s (%d)\n", wszState, wszMessage, iError);
		}
	}
	DebugBreak();
}

bool DataBase::Initialize_DB()
{
	m_retcode = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &m_henv);
	// Set the ODBC version environment attribute  
	if (m_retcode == SQL_SUCCESS || m_retcode == SQL_SUCCESS_WITH_INFO) {
		m_retcode = SQLSetEnvAttr(m_henv, SQL_ATTR_ODBC_VERSION, (SQLPOINTER*)SQL_OV_ODBC3, 0);
		// Allocate connection handle  
		if (m_retcode == SQL_SUCCESS || m_retcode == SQL_SUCCESS_WITH_INFO) {
			m_retcode = SQLAllocHandle(SQL_HANDLE_DBC, m_henv, &m_hdbc);

			// Set login timeout to 5 seconds  
			if (m_retcode == SQL_SUCCESS || m_retcode == SQL_SUCCESS_WITH_INFO) {
				SQLSetConnectAttr(m_hdbc, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0);

				// Connect to data source  
				//ODBC 세팅?
				m_retcode = SQLConnect(m_hdbc, (SQLWCHAR*)L"SimpleMMORPGODBC", SQL_NTS, (SQLWCHAR*)NULL, 0, NULL, 0);

				// Allocate statement handle  
				if (m_retcode == SQL_SUCCESS || m_retcode == SQL_SUCCESS_WITH_INFO) {

					std::cout << "연결 성공" << std::endl;
					return true;
				}
				else
					return false;
			}
			else
				return false;
		}
		else
			return false;
	}
	else
		return false;
}