#pragma once
#ifndef COMMON_STRUCTURES_H_
#include "Common_Structures.h"
#endif
#include <queue>
#include <windows.h>
//#define UNICODE
#include <sqlext.h>
#include <iostream>
//using namespace std;

class DataBase
{
public:
	DataBase();
	~DataBase();

	SQLHENV m_henv;
	SQLHDBC m_hdbc;
	SQLHSTMT m_hstmt = 0;
	SQLRETURN m_retcode;
	SQLWCHAR m_User_Name[MAXNAMESIZE];
	SQLLEN cbName = 0, cbWorldMap = 0;

	void InsertUser(std::wstring name);
	void GetUserInfo(std::wstring name, int& worldinfo);
	void UpdateUserInfo(std::wstring name, int worldinfo);
	void error_print_DB(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode);
	bool Initialize_DB();
	//Db처리를 위한 타임스탬프에따른 우선순위큐 정의 
	//std::priority_queue<>
 };

