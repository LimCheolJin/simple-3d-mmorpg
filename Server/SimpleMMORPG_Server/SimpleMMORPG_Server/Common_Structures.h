#pragma once
//#define _CRT_SECURE_NO_WARNINGS
#pragma comment (lib, "WS2_32.lib")
#pragma comment (lib, "mswsock.lib")

#include <WS2tcpip.h>
#include <MSWSock.h>
#include <mutex>
#include <chrono>
#define UNICODE
#include <sqlext.h>
#include "Protocol.h"



struct ClientSession;
struct DefaultOverlappedEX;
struct OverlappedEX;

enum class Overlapped_Event : unsigned char
{
	Accept = 0,
	Recv,
	Send
};

enum class Database_EventType : unsigned char
{
	Insert = 0,
	SetAndStore
};

struct ClientInfo
{
	wchar_t name[MAXNAMESIZE];
	int hp;
	int mp;
	int level;
	int currentExp;
	int needExp;
	
	
	//정보들 더있으면 추가하기
	int worldmapinfo;  //플레이어가 있는 월드맵의 정보이다 
	ClientInfo() : hp{ 0 }, mp{ 0 }, level{ 0 }, currentExp{ 0 }, needExp{ 0 }, worldmapinfo{ 1 }
	{
		
	}
	ClientInfo(std::wstring stringname) : hp{ 0 }, mp{ 0 }, level{ 0 }, currentExp{ 0 }, needExp{ 0 }, worldmapinfo{ 1 }
	{
		wcscpy(name, stringname.c_str());
	}
	ClientInfo(std::wstring stringname, int worldinfo) : hp{ 0 }, mp{ 0 }, level{ 0 }, currentExp{ 0 }, needExp{ 0 }
	{

	}
	ClientInfo(const ClientInfo& info) : hp{ info.hp }, mp{ info.mp }, level{ info.level }, currentExp{ info.currentExp }, needExp{ info.needExp }, worldmapinfo{ info.worldmapinfo }
	{
		wcscpy(name, info.name);
	}
};
struct DatabaseEvent
{
	ClientInfo clientInfo;
	Database_EventType databaseEventType;
	std::chrono::high_resolution_clock::time_point pushtime;

	DatabaseEvent() = delete;

	DatabaseEvent(std::wstring name, Database_EventType eventType, std::chrono::high_resolution_clock::time_point time) : clientInfo{ name }, databaseEventType{ eventType }, pushtime{time}
	{
		
	}

	DatabaseEvent(std::wstring name, int worldinfo, Database_EventType eventType, std::chrono::high_resolution_clock::time_point time) : clientInfo{ name, worldinfo }, databaseEventType{ eventType }, pushtime{ time }
	{

	}

	DatabaseEvent(const DatabaseEvent& ev) : clientInfo{ ev.clientInfo }, databaseEventType{ ev.databaseEventType }, pushtime{ ev.pushtime }
	{

	}

	const bool operator<(const DatabaseEvent& left) const
	{
		return pushtime > left.pushtime;
	}
};


struct DefaultOverlappedEX
{
	WSAOVERLAPPED overlapped;
	Overlapped_Event overlappedEvent;
	char packetBuffer[MAXBUFFERSIZE];
	WSABUF wsabuffer;
};

struct OverlappedEX : public DefaultOverlappedEX
{
	SOCKET clientSocket;
	unsigned int targetID;
};

class ClientSession
{
public:
	//overlapped
	OverlappedEX m_SendOverlapped;
	OverlappedEX m_RecvOverlapped;

	//session lock(일단 모든 락은 mutex로 해보자. 후에 성능 비교 후 락을 바꾸기)
	std::mutex m_SessionLock; //일단 lock 하나로쓴다. 후에 쪼갠다.

	//client info
	ClientInfo m_Info;

	unsigned int m_SessionIndex; //세션 고유의 인덱스

	SOCKET m_ClientSocket;

	int m_PacketBufferPrevSize = 0; //클라이언트 세션 패킷 버퍼에 들어있는 이전 사이즈
	
	char m_PacketBuffer[MAXBUFFERSIZE];
	
	bool m_CurrentLoginStatus;

	//생성자들
	ClientSession() : m_Info{} { m_CurrentLoginStatus = false; }  //기본생성자 
	ClientSession(const ClientSession& cs) : m_ClientSocket{ cs.m_ClientSocket }, m_Info{ cs.m_Info }, m_SessionIndex{ cs.m_SessionIndex }, m_SendOverlapped{ cs.m_SendOverlapped }, m_RecvOverlapped{ cs.m_RecvOverlapped }, m_CurrentLoginStatus{cs.m_CurrentLoginStatus}{}  //복사 생성자.

	//void Initialize_SendOverlapped();
	//void Initialize_RecvOverlapped();
};

//void ClientSession::Initialize_SendOverlapped()
//{
//	::ZeroMemory(&m_SendOverlapped.overlapped, sizeof(OVERLAPPED));
//	::ZeroMemory(m_SendOverlapped.packetBuffer, MAXBUFFERSIZE);
//	m_SendOverlapped.wsabuffer.buf = m_SendOverlapped.packetBuffer;
//	//m_SendOverlapped.wsabuffer.len = MAXBUFFERSIZE;
//	m_SendOverlapped.overlappedEvent = Overlapped_Event::Send;
//}
//void ClientSession::Initialize_RecvOverlapped()
//{
//	::ZeroMemory(&m_RecvOverlapped.overlapped, sizeof(OVERLAPPED));
//	::ZeroMemory(m_RecvOverlapped.packetBuffer, MAXBUFFERSIZE);
//	m_RecvOverlapped.wsabuffer.buf = m_RecvOverlapped.packetBuffer;
//	m_RecvOverlapped.wsabuffer.len = MAXBUFFERSIZE;
//	m_RecvOverlapped.overlappedEvent = Overlapped_Event::Recv;
//}