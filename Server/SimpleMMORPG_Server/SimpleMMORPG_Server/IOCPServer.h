#pragma once
#pragma comment(lib, "ws2_32")
#pragma comment (lib, "mswsock.lib")
#include <vector>
#include <thread>
#include <unordered_map>
#include <atomic>

#ifndef COMMON_STRUCTURES_H_
#include "Common_Structures.h"
#endif

#ifndef DATABASE_H_
#include "DataBase.h"
#endif


class IOCPServer
{
public:
	IOCPServer();
	~IOCPServer();

	void InitializeServer();
	void Run();
	void Worker_thread();
	void Do_DBTransaction();
	void Disconnect(int user_id);
	void CheckRecvBytesAndCompose(int user_id, int iobytes); // 패킷 조립함수.
	void ProcessPacket(int user_id, char* packetbuf);
	bool CheckLoginDB(int user_id, std::wstring checkid);
	bool CheckCurrentLoginID_In_ClientSessions(std::wstring checkid);
	void CheckClientLogin(int user_id, std::wstring name);
	void ApplyClientInfoFromDatabase(int user_id, std::wstring checkid, int worldinfo);
	//send method
	void SendPacket(int user_id, void* p);
	void SendLoginSucceedPacket(int user_id);
	void SendLoginFailPacket(int user_id);
private:
	SOCKET m_ListenSocket; //리슨 소켓
	HANDLE m_IOCP; // IOCP 핸들러
	
	std::thread* m_DatabaseTransactionThread; //디비용 thread
	DataBase m_DatabaseInstance;


	std::vector<std::thread> m_WorkerThreads; //workerthread 관리용 벡터
	std::unordered_map<unsigned int, ClientSession> m_ClientSessions; //클라세션 관리용 자료구조
	std::priority_queue<DatabaseEvent> m_DatabaseQueue;

	std::atomic_bool m_IsServerRun;   //여러쓰레드에서 끝남을 알기위해 thread safe한 변수사용
	int m_CurrentUserIndex;  // 여러쓰레드에서 접근하기 때문에
	

	//lock
	std::mutex m_ClientSessionManagementLock; //클라이언트 세션을 map에 삽입/삭제를 위한 lock
	std::mutex m_DatabaseQueueLock; // 데이터베이스 큐를 위한 lock
	std::mutex m_DatabaseInstanceLock; // db인스턴스를 위한lock
};

