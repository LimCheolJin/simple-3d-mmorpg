#define _CRT_SECURE_NO_WARNINGS
//#define UNICODE
#include "IOCPServer.h"
#include <iostream>
#include <locale.h>
#include <algorithm>


using namespace std;

IOCPServer::IOCPServer()
{
	m_IsServerRun = false;
	m_CurrentUserIndex = 0;
}


IOCPServer::~IOCPServer()
{

}
void IOCPServer::InitializeServer()
{
	//윈속 초기화, 리슨소켓생성, 포트설정, 프로토콜설정
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 2), &WSAData);

	m_ListenSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);

	SOCKADDR_IN ServerAddr;
	memset(&ServerAddr, 0x00, sizeof(SOCKADDR_IN));
	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_port = htons(9000);
	ServerAddr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	std::cout << "[INFO] Winsock Initialize\n";
	//바인드
	::bind(m_ListenSocket, reinterpret_cast<sockaddr*>(&ServerAddr), sizeof(ServerAddr));
	std::cout << "[INFO] Server Bind\n";
	//리슨
	listen(m_ListenSocket, SOMAXCONN);
	std::cout << "[INFO] Server Bind\n";
	//iocp 생성 //(HANDLE)m_ListenSocket, m_IOCP, 50000, 0)
	m_IOCP = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);
	std::cout << "[INFO] Create IOCP\n";
	//Listensocket IOCP에 등록(완료Key를 50000으로)
	CreateIoCompletionPort((HANDLE)m_ListenSocket, m_IOCP, 50000, 0);
	std::cout << "[INFO] Enter Listensocket in IOCP\n";
	//acceptEX를 위함.
	SOCKET socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	OverlappedEX accept_overlapped;
	::ZeroMemory(&accept_overlapped.overlapped, sizeof(accept_overlapped.overlapped));
	accept_overlapped.overlappedEvent = Overlapped_Event::Accept;
	accept_overlapped.clientSocket = socket; //소켓 재사용을 위함.
	AcceptEx(m_ListenSocket, socket, accept_overlapped.packetBuffer, NULL, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, NULL, &accept_overlapped.overlapped); //소켓을 만들어서 리턴하지않음, 미리 소켓을 만들어놓고 이 만들어논 소켓을 클라이언트와연결해줌.
	
	std::cout << "[INFO] Server is ready All\n";
	setlocale(LC_ALL, "Korean");


	if (m_DatabaseInstance.Initialize_DB())
	{
		std::cout << "[INFO] DB Server Initalize complete \n";
		Run();
	}
	else
		std::cout << "[INFO] DB Server cannot Initialize Error\n";
}
void IOCPServer::Run()
{
	m_IsServerRun = true;
	
	//db 쓰레드시작
	m_DatabaseTransactionThread = new std::thread{ &IOCPServer::Do_DBTransaction, this };

	//worker 쓰레드들 시작
	//시스템의 코어 수 x 2 만큼 넉넉히.
	SYSTEM_INFO info{};
	GetSystemInfo(&info);
	std::cout << "[INFO] " << info.dwNumberOfProcessors << "개의 worker thread on\n";
	for (int i = 0; i < info.dwNumberOfProcessors; ++i)
	{
		m_WorkerThreads.emplace_back( &IOCPServer::Worker_thread, this );
	}


	//종료대기
	for (auto& thread : m_WorkerThreads)
		thread.join();
	m_DatabaseTransactionThread->join();
	delete m_DatabaseTransactionThread;

	//메모리 해제할게 있으면 해제 하기

}
void IOCPServer::Disconnect(int user_id)
{ //이것도 괴상하다.
	bool disconnectStatus = false; 
	//여러번 disconnect 호출을 방지하기위함.
	//필요없는 db접근을 막기 위함이다.


	m_ClientSessions[user_id].m_SessionLock.lock();
	ClientInfo cs = m_ClientSessions[user_id].m_Info;
	m_ClientSessions[user_id].m_SessionLock.unlock();


	m_ClientSessionManagementLock.lock();
	if (m_ClientSessions.find(user_id) != m_ClientSessions.end())
	{
		m_ClientSessions.erase(user_id);
		m_ClientSessionManagementLock.unlock();
		std::cout << "[INFO] User Index: " << user_id << " 종료\n";

		disconnectStatus = true;
	}
	else
	{
		m_ClientSessionManagementLock.unlock();		
		disconnectStatus = false;
	}

	if (cs.name == L""); return;  //암것도 안하고 꺼버릴떄 로그인 버튼조차 안눌렀을떄 DB에 접근할필요가 없다. 
	if (disconnectStatus)
	{
		//m_DatabaseInstanceLock.lock();
		m_DatabaseQueueLock.lock();
		m_DatabaseQueue.push(DatabaseEvent{cs.name, cs.worldmapinfo, Database_EventType::SetAndStore, std::chrono::high_resolution_clock::now()});
		m_DatabaseQueueLock.unlock();
		
		//m_DatabaseInstanceLock.unlock();
	}

}
void IOCPServer::Worker_thread()
{
	while (m_IsServerRun)
	{
		//gqcs EX 써보기 일단은 GQCS로 후에 EX
		DWORD iobytes;
		ULONG_PTR key;
		WSAOVERLAPPED* over;

		
		GetQueuedCompletionStatus(m_IOCP, &iobytes, &key, &over, INFINITE);

		//바꿔치기 
		OverlappedEX* overlappedEX = reinterpret_cast<OverlappedEX*>(over);
		int user_id =(int)key;

		switch (overlappedEX->overlappedEvent)
		{
		case Overlapped_Event::Accept:
		{
			SOCKET completionSocket = overlappedEX->clientSocket;

			//std::cout << "[INFO] 유저 접속\n";
			ClientSession cs{};

			//인터락을 고민해보기 사용법도 숙지하기.
			cs.m_ClientSocket = completionSocket;
			/*cs.m_RecvOverlapped.wsabuffer.buf = cs.m_RecvOverlapped.packetBuffer;
			cs.m_RecvOverlapped.wsabuffer.len = MAXBUFFERSIZE;
			cs.m_SendOverlapped.wsabuffer.buf = cs.m_SendOverlapped.packetBuffer;*/

			
			m_ClientSessionManagementLock.lock();
			cs.m_SessionIndex = m_CurrentUserIndex++; //이게 아토믹한지 잘모르겠음 고민해보기
			m_ClientSessions.insert(std::pair<unsigned int, ClientSession>(cs.m_SessionIndex, cs));
			m_ClientSessionManagementLock.unlock();

			//새로들어온놈 IOCP 등록
			CreateIoCompletionPort(reinterpret_cast<HANDLE>(completionSocket), m_IOCP , cs.m_SessionIndex, 0);

			//새로들어온놈을 등록했으니 Recv받을수있도록 하자. 
			DWORD flags = 0;
			::ZeroMemory(&m_ClientSessions[cs.m_SessionIndex].m_RecvOverlapped.overlapped, sizeof(OVERLAPPED));
			::ZeroMemory(&m_ClientSessions[cs.m_SessionIndex].m_SendOverlapped.overlapped, sizeof(OVERLAPPED));
			m_ClientSessions[cs.m_SessionIndex].m_RecvOverlapped.overlappedEvent = Overlapped_Event::Recv;
			m_ClientSessions[cs.m_SessionIndex].m_RecvOverlapped.wsabuffer.buf = m_ClientSessions[cs.m_SessionIndex].m_RecvOverlapped.packetBuffer;
			m_ClientSessions[cs.m_SessionIndex].m_RecvOverlapped.wsabuffer.len = MAXBUFFERSIZE;
			m_ClientSessions[cs.m_SessionIndex].m_SendOverlapped.wsabuffer.buf = m_ClientSessions[cs.m_SessionIndex].m_SendOverlapped.packetBuffer;
			WSARecv(m_ClientSessions[cs.m_SessionIndex].m_ClientSocket, &m_ClientSessions[cs.m_SessionIndex].m_RecvOverlapped.wsabuffer, 1, NULL, &flags, &m_ClientSessions[cs.m_SessionIndex].m_RecvOverlapped.overlapped, NULL);

			std::cout << "[INFO] user: " << cs.m_SessionIndex << " 접속\n";
			
			//소켓 재활용 다시 accept 를 위함.
			completionSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
			overlappedEX->clientSocket = completionSocket;
			::ZeroMemory(&overlappedEX->overlapped, sizeof(OVERLAPPED));
			AcceptEx(m_ListenSocket, completionSocket, overlappedEX->packetBuffer, NULL, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, NULL, &overlappedEX->overlapped);
		}
		break;

		case Overlapped_Event::Recv:
		{
			if (0 == iobytes)
				Disconnect(user_id);
			else
			{
				
				CheckRecvBytesAndCompose(user_id, iobytes);
				::ZeroMemory(&m_ClientSessions[user_id].m_RecvOverlapped.overlapped, sizeof(OVERLAPPED));

				DWORD flags = 0;
				WSARecv(m_ClientSessions[user_id].m_ClientSocket, &m_ClientSessions[user_id].m_RecvOverlapped.wsabuffer, 1, NULL, &flags, &m_ClientSessions[user_id].m_RecvOverlapped.overlapped, NULL);

			}
		}
		break;

		case Overlapped_Event::Send:
		{
			if (0 == iobytes)
				Disconnect(user_id);
			else
			{
				::ZeroMemory(&m_ClientSessions[user_id].m_SendOverlapped.overlapped, sizeof(OVERLAPPED));
				::ZeroMemory(m_ClientSessions[user_id].m_SendOverlapped.packetBuffer, iobytes);
			}
		}
		break;

		default:
			std::cout << "Unknown overlapped event type !!\n";
			DebugBreak();
			break;
		}

		
	}
}
void IOCPServer::Do_DBTransaction()
{
	while (m_IsServerRun)
	{
		//DB SP사용
		m_DatabaseQueueLock.lock();
		if (m_DatabaseQueue.empty())
		{
			m_DatabaseQueueLock.unlock();
			continue;
		}
		
		//m_DatabaseQueueLock.lock();
		DatabaseEvent ev = m_DatabaseQueue.top();
		m_DatabaseQueue.pop();
		m_DatabaseQueueLock.unlock();

		switch (ev.databaseEventType)
		{
		case Database_EventType::Insert:
		{
			m_DatabaseInstanceLock.lock();
			m_DatabaseInstance.InsertUser(std::wstring( ev.clientInfo.name) );
			m_DatabaseInstanceLock.unlock();
			//ev.clientInfo.name
		}
		break;

		case Database_EventType::SetAndStore:
		{
			m_DatabaseInstanceLock.lock();
			m_DatabaseInstance.UpdateUserInfo(ev.clientInfo.name, ev.clientInfo.worldmapinfo);
			m_DatabaseInstanceLock.unlock();
		}
		break;
		}
	}
}

void IOCPServer::CheckRecvBytesAndCompose(int user_id, int iobytes)
{
	ClientSession& cu = m_ClientSessions[user_id];
	OverlappedEX& r_o = cu.m_RecvOverlapped;

	//패킷버퍼의 0번째 인덱스인 사이즈를 검사할 필요가 있는것 같다. 아니면 클라이언트 맘대로보냈을때 제어가 안될거같음
	int rest_bytes = iobytes;
	char* p = r_o.packetBuffer;
	int packet_size = 0;
	if (0 != cu.m_PacketBufferPrevSize) packet_size = cu.m_PacketBuffer[0];
	while (rest_bytes > 0) // 아직처리할 데이터가 남아있으면.
	{
		if (0 == packet_size)
			packet_size = *p;
		if (packet_size <= rest_bytes + cu.m_PacketBufferPrevSize) //지난번에 받은거랑 지금받은거랑 합쳣을때 패킷사이즈보다 크면 완성가능
		{
			memcpy(cu.m_PacketBuffer + cu.m_PacketBufferPrevSize, p, packet_size - cu.m_PacketBufferPrevSize);
			p += packet_size - cu.m_PacketBufferPrevSize;
			rest_bytes -= packet_size - cu.m_PacketBufferPrevSize;
			packet_size = 0;
			ProcessPacket(user_id, cu.m_PacketBuffer);
			cu.m_PacketBufferPrevSize = 0;
		}
		else //완성못하면 저장을해야함.
		{
			memcpy(cu.m_PacketBuffer + cu.m_PacketBufferPrevSize, p, rest_bytes);
			cu.m_PacketBufferPrevSize += rest_bytes; // 추가해줌 이전에받았던 데이터사이즈를
			rest_bytes = 0;
			p += rest_bytes;

		}
	}
}
void IOCPServer::CheckClientLogin(int user_id, std::wstring name)
{
	// 현재 접속중인지 확인.
	if (!CheckCurrentLoginID_In_ClientSessions(name))
	{
		SendLoginFailPacket(user_id);
		m_ClientSessions[user_id].m_SessionLock.lock(); //여기락이필요할까?
		::ZeroMemory(&m_ClientSessions[user_id].m_Info, sizeof(ClientInfo));
		m_ClientSessions[user_id].m_SessionLock.unlock();
		return;
	}

	if (CheckLoginDB(user_id, name))
	{
		//로그인 ok packet 보내기
		SendLoginSucceedPacket(user_id);
		
	}
	//else
	//{
	//	//로그인 fail packet 보내기
	//	SendLoginFailPacket(user_id);
	//}
}

void IOCPServer::ProcessPacket(int user_id, char* packetbuf)
{
	e_packet_type type = static_cast<e_packet_type>(packetbuf[1]);
	switch (type)
	{
	case e_packet_type::LOGIN_PACKET:
	{
		CS_LOGIN_PACKET* p = reinterpret_cast<CS_LOGIN_PACKET*>(packetbuf);
		std::wstring name{ p->id };

		CheckClientLogin(user_id, name);
	}
	break;
	case e_packet_type::LOGOUT_PACKET:
	{
		Disconnect(user_id);
	}
	break;
	default:
	{
		std::cout << "unknown packet type \n";
		DebugBreak();
	}
	break;
	}
}
bool IOCPServer::CheckCurrentLoginID_In_ClientSessions(std::wstring checkid)
{
	//2020_10_15 이게 맞는지 모르겠음... lock이 일단 너무큼.
	//find_if했을때 각 클라이언트 세션들이 thread_safe하지 않을거같다.

	if (checkid == L"") return false; //로그인 버튼을 눌러서 확인을 하려고하지만 공백일경우엔 false

	m_ClientSessionManagementLock.lock();
	auto iter = find_if(m_ClientSessions.begin(), m_ClientSessions.end(), [&](const std::pair<unsigned int, ClientSession>& cl) { return cl.second.m_Info.name == checkid;});
	if (iter == m_ClientSessions.end())
	{
		m_ClientSessionManagementLock.unlock();
		return true;
	}
	else
	{
		m_ClientSessionManagementLock.unlock();
		return false;
	}


	/*for (auto cl : m_ClientSessions)
	{
		if (cl.second.m_Info.name == checkid)
			return false;
	}*/
	
	

	//return true;
}

bool IOCPServer::CheckLoginDB(int user_id, std::wstring checkid)
{
	//DB에 접근해서 없는 아이디면 INSERT
	//판단은 월드 레이어로 하자. 월드에 존재하는 아이디면 get
	int worldinfo = -1;

	m_DatabaseInstanceLock.lock();
	m_DatabaseInstance.GetUserInfo(checkid, worldinfo);
	m_DatabaseInstanceLock.unlock();

	if (-1 == worldinfo) // db에 없는 정보이다. 새로 들어왔다.
	{
		//여기서 db락을 걸고 푸쉬를 하자 큐에

		DatabaseEvent ev = DatabaseEvent{ checkid, Database_EventType::Insert, std::chrono::high_resolution_clock::now() };
		m_DatabaseQueueLock.lock();
		m_DatabaseQueue.push(ev);
		m_DatabaseQueueLock.unlock();

		m_ClientSessions[user_id].m_SessionLock.lock();
		m_ClientSessions[user_id].m_Info = ev.clientInfo;
		m_ClientSessions[user_id].m_SessionLock.unlock();
	}
	else
	{
		//존재 하는 놈이다. 정보 가져온것을 클라이언트 세션에 적용하자.
		ApplyClientInfoFromDatabase(user_id, checkid, worldinfo);
	}
	// 존재하지 않는아이디면 insert 

	//있는 아이디면 정보 GET 

	//INSERT,set 이벤트는 큐로처리하자 

	//Get 해오는 것만 그냥하자.
	return true;
}
void IOCPServer::ApplyClientInfoFromDatabase(int user_id, std::wstring checkid, int worldinfo)
{
	m_ClientSessions[user_id].m_SessionLock.lock();
	wcscpy(m_ClientSessions[user_id].m_Info.name, checkid.c_str());
	m_ClientSessions[user_id].m_Info.worldmapinfo = worldinfo;
	m_ClientSessions[user_id].m_SessionLock.unlock();
}
void IOCPServer::SendPacket(int user_id, void* p)
{
	char* packet = reinterpret_cast<char*>(p);
	int packet_size = packet[0];
	memcpy(m_ClientSessions[user_id].m_SendOverlapped.packetBuffer, p, packet_size);
	m_ClientSessions[user_id].m_SendOverlapped.wsabuffer.len = packet_size;
	m_ClientSessions[user_id].m_SendOverlapped.overlappedEvent = Overlapped_Event::Send;

	WSASend(m_ClientSessions[user_id].m_ClientSocket, &m_ClientSessions[user_id].m_SendOverlapped.wsabuffer,1, NULL, 0, &m_ClientSessions[user_id].m_SendOverlapped.overlapped, NULL);
		//WSASend(u.m_s, &exover->wsabuf, 1, NULL, 0, &exover->over, NULL);
}
void IOCPServer::SendLoginSucceedPacket(int user_id)
{
	SC_LOGIN_SUCCEED_PACKET p{};
	p.size = sizeof(p);
	p.type = e_packet_type::LOGIN_SUCCEED;
	p.user_id = user_id;

	SendPacket(user_id, &p);
}
void IOCPServer::SendLoginFailPacket(int user_id)
{
	SC_LOGIN_FAIL_PACKET p{};
	p.size = sizeof(p);
	p.type = e_packet_type::LOGIN_FAIL;

	SendPacket(user_id, &p);
}