#pragma once
#define _CRT_SECURE_NO_WARNINGS
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#pragma comment(lib, "ws2_32")
#pragma comment (lib, "mswsock.lib")
#include <iostream>
#include <WS2tcpip.h>
#include <MSWSock.h>
#include <vector>
#include <thread>
#include <string>
#include "Protocol.h"

constexpr int MAX_CLIENT = 10000;
constexpr int MAX_BUFFER_SIZE = 4096;
enum class NETWORK_EVENT : unsigned char
{
	CONNECT,
	SEND,
	RECV
};

struct OverlappedEX
{
	WSAOVERLAPPED over;
	WSABUF wsaBuf;
	char userBuf[MAX_BUFFER_SIZE];
	NETWORK_EVENT ioevent;
};
class DummyClient
{
public:
	OverlappedEX sendover;
	OverlappedEX recvover;
	char packetbuf[MAX_BUFFER_SIZE];
	SOCKET clientSocket;
	int prevsize = 0;
};


HANDLE g_iocp;
DummyClient g_clients[MAX_CLIENT];

void process_packet(int user_id, char* buf)
{
	switch (e_packet_type( buf[1]))
	{
	case e_packet_type::LOGIN_FAIL:
	{
		SC_LOGIN_FAIL_PACKET* p = reinterpret_cast<SC_LOGIN_FAIL_PACKET*>(buf);

	}
	break;

	case e_packet_type::LOGIN_SUCCEED:
	{
		SC_LOGIN_SUCCEED_PACKET* p = reinterpret_cast<SC_LOGIN_SUCCEED_PACKET*>(buf);
	}
	break;
	}
}
void construct_packet(int user_id, int iobytes)
{
	DummyClient& cu = g_clients[user_id];
	OverlappedEX& r_o = cu.recvover;

	//패킷버퍼의 0번째 인덱스인 사이즈를 검사할 필요가 있는것 같다. 아니면 클라이언트 맘대로보냈을때 제어가 안될거같음
	int rest_bytes = iobytes;
	char* p = r_o.userBuf;
	int packet_size = 0;
	if (0 != cu.packetbuf) packet_size = cu.packetbuf[0];
	while (rest_bytes > 0) // 아직처리할 데이터가 남아있으면.
	{
		if (0 == packet_size)
			packet_size = *p;
		if (packet_size <= rest_bytes + cu.prevsize) //지난번에 받은거랑 지금받은거랑 합쳣을때 패킷사이즈보다 크면 완성가능
		{
			memcpy(cu.packetbuf + cu.prevsize, p, packet_size - cu.prevsize);
			p += packet_size - cu.prevsize;
			rest_bytes -= packet_size - cu.prevsize;
			packet_size = 0;
			process_packet(user_id, cu.packetbuf);
			cu.prevsize = 0;
		}
		else //완성못하면 저장을해야함.
		{
			memcpy(cu.packetbuf + cu.prevsize, p, rest_bytes);
			cu.prevsize += rest_bytes; // 추가해줌 이전에받았던 데이터사이즈를
			rest_bytes = 0;
			p += rest_bytes;

		}
	}
}
void sendpacket(int user_id, void* p)
{
	char* packet = reinterpret_cast<char*>(p);
	int packet_size = packet[0];
	memcpy(g_clients[user_id].sendover.userBuf, p, packet_size);
	g_clients[user_id].sendover.wsaBuf.len = packet_size;
	//g_clients[user_id].sendover.overlappedEvent = Overlapped_Event::Send;

	WSASend(g_clients[user_id].clientSocket, &g_clients[user_id].sendover.wsaBuf, 1, NULL, 0, &g_clients[user_id].sendover.over, NULL);
}
void worker_thread()
{
	while (true)
	{
		DWORD io_size;
		OverlappedEX* over;
		ULONG_PTR key;
		BOOL ret = GetQueuedCompletionStatus(g_iocp, &io_size, &key, reinterpret_cast<LPWSAOVERLAPPED*>(&over), INFINITE);

		OverlappedEX* overlappedEX = reinterpret_cast<OverlappedEX*>(over);
		int user_id = (int)key;
		switch (overlappedEX->ioevent)
		{
		case NETWORK_EVENT::RECV:
		{
			if (io_size == 0)
			{

			}
			else
			{
				construct_packet(user_id, io_size);
				::ZeroMemory(&g_clients[user_id].recvover.over, sizeof(WSAOVERLAPPED));

				DWORD flags = 0;
				WSARecv(g_clients[user_id].clientSocket, &g_clients[user_id].recvover.wsaBuf, 1, NULL, &flags, &g_clients[user_id].recvover.over, NULL);
			}
		}
		break;

		case NETWORK_EVENT::SEND:
		{
			if (io_size == 0)
			{

			}
			else
			{
				::ZeroMemory(&g_clients[user_id].sendover.over, sizeof(WSAOVERLAPPED));
				::ZeroMemory(g_clients[user_id].sendover.userBuf, io_size);
			}
		}
		break;

		default:
		{
			std::cout << "Unknown overlapped event type !!\n";
			DebugBreak();
		}
		break;
		}
	}
}

void connectFunction()
{
	for (int i = 0; i < MAX_CLIENT; ++i)
	{
		g_clients[i].clientSocket = WSASocketW(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);

		SOCKADDR_IN ServerAddr;
		ZeroMemory(&ServerAddr, sizeof(SOCKADDR_IN));
		ServerAddr.sin_family = AF_INET;
		ServerAddr.sin_port = htons(9000);
		ServerAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
		int Result = WSAConnect(g_clients[i].clientSocket, (sockaddr*)&ServerAddr, sizeof(ServerAddr), NULL, NULL, NULL, NULL);
		if (0 != Result) {
			std::cout << "WSA Connect Error\n";
			DebugBreak();
		}

		::ZeroMemory(&g_clients[i].recvover.over, sizeof(WSAOVERLAPPED));
		::ZeroMemory(&g_clients[i].sendover.over, sizeof(WSAOVERLAPPED));
		g_clients[i].recvover.wsaBuf.buf = g_clients[i].recvover.userBuf;
		g_clients[i].recvover.wsaBuf.len = MAX_BUFFER_SIZE;
		g_clients[i].recvover.ioevent = NETWORK_EVENT::RECV;
		g_clients[i].sendover.wsaBuf.buf = g_clients[i].sendover.userBuf;
		g_clients[i].sendover.ioevent = NETWORK_EVENT::SEND;
		::ZeroMemory(g_clients[i].packetbuf, MAX_BUFFER_SIZE);
		g_clients[i].prevsize = 0;
		
		DWORD recv_flag = 0;
		CreateIoCompletionPort(reinterpret_cast<HANDLE>(g_clients[i].clientSocket), g_iocp, i, 0);
		
		CS_LOGIN_PACKET p{};
		std::wstring s = L"DummyClient" + std::to_wstring(i) + L"10월17일";
		wcscpy(p.id, s.c_str() );
		p.size = sizeof(p);
		p.type = e_packet_type::LOGIN_PACKET;

		sendpacket(i, &p);

		int ret = WSARecv(g_clients[i].clientSocket, &g_clients[i].recvover.wsaBuf, 1, NULL, &recv_flag, &g_clients[i].recvover.over, NULL);
	}
}

int main()
{
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 2), &WSAData);

	std::vector<std::thread> worker_threads;

	

	g_iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, 0, NULL, 0);

	std::thread connectthread{ connectFunction };
	for (int i = 0; i < 1; ++i)
		worker_threads.emplace_back( std::thread{worker_thread} );

	connectthread.join();
	for (auto& thread : worker_threads)
	{
		thread.join();
	}
	return 0;
}